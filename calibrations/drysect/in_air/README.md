Calibration of Drysect in air with Commonlands 2.7mm f2.8 lenses [CIL027-F2.8-M12ANIR](https://commonlands.com/products/low-distortion-m12-lens-cil027?variant=40049436688502)

Data and analysis int [raven_analysis/2023-09-28_drysect_first_calibration](https://gitlab.com/rsa-perception-sensor/trisect_analysis/-/tree/master/2023-09-28_drysect_first_calibration?ref_type=heads)
