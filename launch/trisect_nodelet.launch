<?xml version="1.0" encoding="utf-8"?>

<!-- This is the primary entrypoint for the Trisect software.
     It launches the full Trisect stack, using nodelets for efficient data transfer. -->

<launch>

<!-- ============== Top-level configuration =============== -->

<!-- Sets the top-level of the ROS topic tree -->
<arg name="root_ns" default="trisect"/>

<!-- "shortcut" configuration for Drysect:
        disables color camera
        sets environment to "in_air"
-->
<arg name="is_drysect" default="false" />

<!-- ============== Camera configuration =============== -->

<!-- If true, the hardware camera driver is run,
     Set to false to only run the postprocessing (stereo, darknet) toolchain -->
<arg name="do_cameras" default="true"/>

<!-- Configuration for monchrome stereo cameras -->
<arg name="stereo_fps" default="10" />
<arg name="stereo_gain" default="1800" />
<arg name="stereo_exposure_max_ms" default="20"/>

<!-- bit depth for monochrome stereo images.  Valid values: 8, 10, 12 -->
<arg name="stereo_bit_depth" default="8"/>

<arg name="software_trigger" default="false"/>

<arg name="stereo_h_flip" default="false" unless="$(arg is_drysect)"/>
<arg name="stereo_h_flip" default="true"  if="$(arg is_drysect)"/>
<arg name="stereo_v_flip" default="false" unless="$(arg is_drysect)"/>
<arg name="stereo_v_flip" default="true"  if="$(arg is_drysect)"/>

<arg name="do_color" default="true" unless="$(arg is_drysect)"/>
<arg name="do_color" default="false" if="$(arg is_drysect)"/>

<!-- Configuration for color cameras -->
<arg name="color_fps" default="10" />

<!-- Color camera resolutions are set using an enum corresponding to the
     IMX477's sensor modes:

     0: 3840 x 2160
     1: 1920 x 1080
-->
<arg name="color_resolution" default="0" />

<!-- ============== Stereo block matching configuration =============== -->

<!-- If true, perform stereo calculations -->
<arg name="do_stereo" default="true"/>

<!-- Specifies which calibration files to use -->
<arg name="environment" default="in_water" unless="$(arg is_drysect)"/>
<arg name="environment" default="in_air"   if="$(arg is_drysect)"/>

  <!-- Valid values are:
           "raft"  uses RAFT-Stereo
           "vpi"   Uses VPI
           "raft"  Uses raft_stereo (must be run in a rosEnv environment)
-->
<arg name="stereo_matcher" default="raft" />

<arg name="disparity_debug" default="false" />

<!-- ============== I2C sensor configuration =============== -->

<arg name="do_sensors" default="true" unless="$(arg is_drysect)"/>
<arg name="do_sensors" default="false" if="$(arg is_drysect)"/>

<!-- ============== web_video_server configuration =============== -->

<arg name="web_server_port" default="8080"/>

<arg name="do_foxglove" default="true"/>

<!-- ============== Internal configuration =============== -->

<arg name="host" default="$(env HOSTNAME)"/>
<arg name="manager" default="/$(arg root_ns)/trisect_nodelet_manager" />
<arg name="respawn" default="false" />
<arg name="manager_launch_prefix" default=""/>

<arg     if="$(arg respawn)" name="bond" value="" />
<arg unless="$(arg respawn)" name="bond" value="--no-bond" />

<!-- ============================================= -->
<!-- ============== Start of nodes =============== -->


<group ns="$(arg root_ns)">

  <!-- Nodelet manager -->
  <node name="trisect_nodelet_manager" type="nodelet" pkg="nodelet"
          launch-prefix="$(arg manager_launch_prefix)"
          args="manager" output="screen"/>

  <!-- The main camera driver -->
  <node pkg="nodelet" type="nodelet" name="camera_driver" output="screen"
          args="load trisect_camera_driver/nodelet $(arg manager)"
          if="$(arg do_cameras)">
    <param name="stereo_fps"   value="$(arg stereo_fps)" />
    <param name="stereo_gain"  value="$(arg stereo_gain)" />
    <param name="stereo_exposure_max_ms" value="$(arg stereo_exposure_max_ms)" />
    <param name="link_exposures" value="true" />
    <param name="auto_exposure" value="true" />

    <param name="stereo_bit_depth" value="$(arg stereo_bit_depth)"/>
    <param name="stereo_software_trigger" value="$(arg software_trigger)" />

    <param name="is_drysect" value="$(arg is_drysect)"/>

    <param name="stereo_v_flip" value="$(arg stereo_v_flip)"/>
    <param name="stereo_h_flip" value="$(arg stereo_h_flip)"/>

    <param name="do_color" value="$(arg do_color)"/>

    <param name="color_fps" value="$(arg color_fps)" />
    <param name="color_resolution" value="$(arg color_resolution)" />

    <param name="left_camera_info" value="file://$(find trisect_sw)/calibrations/$(arg host)/$(arg environment)/left_stereo_params.yaml" />
    <param name="right_camera_info" value="file://$(find trisect_sw)/calibrations/$(arg host)/$(arg environment)/right_stereo_params.yaml" />

    <!-- Odd syntax for remapping just the image topic from a CameraPublisher
          Leave the camera_info in place -->
    <remap from="rectified/left/image_rect" to="left/rect/image_rect" />
    <remap from="left/rect/camera_info" to="rectified/left/camera_info" />
    <remap from="left/rect/image_rect" to="left/image_rect" />

    <remap from="rectified/right/image_rect" to="right/rect/image_rect" />
    <remap from="right/rect/camera_info" to="rectified/right/camera_info" />
    <remap from="right/rect/image_rect" to="right/image_rect" />
  </node>

  <node name="video_server" pkg="nodelet" type="nodelet"
        args="load trisect_web_video_server/nodelet $(arg manager)">
        <param name="port" value="$(arg web_server_port)"/>
  </node>

  <!-- <node name="stereo_watchdog" type="watchdog" pkg="stereo_watchdog" output="screen" if="$(arg do_cameras)">
    <remap from="left_camera_info" to="left/camera_info" />
    <remap from="right_camera_info" to="right/camera_info" />

    <param name="max_sync_delta" value="0.01" />
  </node> -->

  <node pkg="ros_jetson_stats" type="jetson_stats.py" name="ros_jetson_stats" output="screen"/>

  <!-- Enable a base diagnostic aggregator to read the status of your NVIDIA Jetson -->
  <node pkg="diagnostic_aggregator" type="aggregator_node" name="diagnostic_aggregator" clear_params="true">
    <rosparam command="load" file="$(find ros_jetson_stats)/param/jtop.yaml"/>
  </node>

  <group if="$(arg do_stereo)">
    <include if="$(eval stereo_matcher=='raft')"
            file="$(find trisect_sw)/launch/raft_stereo.xml" pass_all_args="true"/>

    <include if="$(eval stereo_matcher=='vpi')"
            file="$(find trisect_sw)/launch/vpi_stereo.xml" pass_all_args="true"/>

    <!-- Don't need disparity_visualize and pointclouds is we're only rectifying-->
    <include file="$(dirname)/stereo_common_nodelets.xml" pass_all_args="true" />
  </group>

  <group ns="sensors" if="$(arg do_sensors)">
    <node pkg="trisect_i2c_sensors" type="trisect_i2c_sensors" name="trisect_i2c_sensors"
        output="screen" />
  </group>

  <include file="$(dirname)/disable_image_transport_plugins.xml" />

</group>  <!-- Group "trisect" -->

    <node pkg="nodelet" type="nodelet" name="foxglove_bridge"
          args="load foxglove_bridge/foxglove_bridge_nodelet $(arg manager)"
          if="$(arg do_foxglove)">
      <param name="use_compression" value="false"/>
    </node>

</launch>
