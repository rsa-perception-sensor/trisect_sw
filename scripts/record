#!/usr/bin/env python3

import argparse
import subprocess
import signal

# \todo(AMM)  Reduce the DRY

LEFT_TOPICS = [
    "/trisect/left/image_raw",
    "/trisect/left/camera_info",
    "/trisect/left/imaging_metadata",
]

LEFT_COMPRESSED_TOPICS = [
    "/trisect/left/image_raw/compressed",
    "/trisect/left/camera_info",
    "/trisect/left/imaging_metadata",
]

RIGHT_TOPICS = [
    "/trisect/right/image_raw",
    "/trisect/right/camera_info",
    "/trisect/right/imaging_metadata",
]

RIGHT_COMPRESSED_TOPICS = [
    "/trisect/right/image_raw/compressed",
    "/trisect/right/camera_info",
    "/trisect/right/imaging_metadata",
]

COLOR_TOPICS = ["/trisect/color/image_raw", "/trisect/color/camera_info"]

COLOR_COMPRESSED_TOPICS = [
    "/trisect/color/image_raw/compressed",
    "/trisect/color/camera_info",
]

DISPARITY_TOPICS = [
    "/trisect/downsampled/disparity",
    "/trisect/downsampled/disparity/image",
    "/trisect/downsampled/left/camera_info",
    "/trisect/downsampled/right/camera_info",
]

DOWNSAMPLED_RECTIFIED_TOPICS = [
    "/trisect/downsampled/left/camera_info",
    "/trisect/downsampled/left/image_rect",
    "/trisect/downsampled/right/camera_info",
    "/trisect/downsampled/right/image_rect",
]

DEPTH_TOPICS = ["/trisect/downsampled/depth"]

POINTCLOUD_TOPICS = ["/trisect/downsampled/points2"]


SENSOR_TOPICS = [
    "/trisect/sensors/raw_data",
    "/trisect/sensors/imu",
    "/trisect/sensors/magnetometer",
    "/trisect/sensors/pressure",
    "/trisect/sensors/relative_humidity",
    "/trisect/sensors/temperature",
    "/trisect/sensors/leak",
    "/trisect/sensors/leak_voltage",
]
ALWAYS_TOPICS = [
    "/diagnostics",
    "/trisect/tegrastats",
    "/trisect/tegrastats_raw",
    "/rosout",
]

valid_keywords = [
    "left",
    "right",
    "stereo",
    "color",
    "cameras",
    "disparity",
    "depth",
    "pointcloud",
    "all",
    "rtabmap_depth",
    "downsampled",
]


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Rosbag record trisect data.")
    parser.add_argument(
        "topics",
        metavar="topic",
        nargs="+",
        help="keyword or topic name (valid keywords: %s)" % ",".join(valid_keywords),
    )

    parser.add_argument(
        "--compressed",
        action="store_true",
        default=False,
        help="Save the *compressed* image topics",
    )

    parser.add_argument(
        "-d",
        "--duration",
        default=None,
        metavar="DURATION",
        help="Record rosbag for duration.  DURATION in seconds, unless 'm', or 'h' is appended.",
    )

    parser.add_argument(
        "--split",
        default=False,
        action="store_true",
        help="split the bag when maximum size or duration is reached (use with --size or --duration)",
    )

    parser.add_argument(
        "--size", default=None, type=int, help="record a bag of maximum size SIZE MB"
    )

    args = parser.parse_args()

    topics = []

    for topic in args.topics:

        if topic.startswith("/"):
            topics.append(topic)
            continue
        else:

            if topic not in valid_keywords:
                print('Invalid keyword "%s"' % topic)
                exit()

            if topic in ["left", "stereo", "cameras", "all"]:
                if args.compressed:
                    topics = topics + LEFT_COMPRESSED_TOPICS
                else:
                    topics = topics + LEFT_TOPICS

            if topic in ["right", "stereo", "cameras", "all"]:
                if args.compressed:
                    topics = topics + RIGHT_COMPRESSED_TOPICS
                else:
                    topics = topics + RIGHT_TOPICS

            if topic in ["color", "cameras", "all"]:
                if args.compressed:
                    topics = topics + COLOR_COMPRESSED_TOPICS
                else:
                    topics = topics + COLOR_TOPICS

            if topic in ["downsampled", "all"]:
                topics += DOWNSAMPLED_RECTIFIED_TOPICS

            if topic in ["disparity", "all"]:
                topics = topics + DISPARITY_TOPICS

            if topic in ["pointcloud", "all"]:
                topics = topics + POINTCLOUD_TOPICS

            if topic in ["depth", "all"]:
                topics = topics + DEPTH_TOPICS

            if topic in ["rtabmap_depth", "all"]:
                topics = (
                    topics
                    + DEPTH_TOPICS
                    + DISPARITY_TOPICS
                    + DOWNSAMPLED_RECTIFIED_TOPICS
                )

    topics = topics + ALWAYS_TOPICS + SENSOR_TOPICS
    topics = list(set(topics))
    print("Recording these topics: %s" % " ".join(topics))

    record_args = []

    if args.duration:
        record_args += [f"--duration={args.duration}"]

    if args.split:
        record_args += ["--split"]

    if args.size:
        record_args += [f"--size={args.size}"]

    try:
        p = subprocess.Popen(
            ["rosbag", "record", "--buffsize=2048"] + record_args + topics
        )
        p.wait()
    except KeyboardInterrupt:
        p.send_signal(signal.SIGINT)
        p.wait()
